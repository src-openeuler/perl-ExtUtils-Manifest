Name:           perl-ExtUtils-Manifest
Epoch:          1
Version:        1.75
Release:        2
Summary:        Utilities to write and check a MANIFEST file
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/ExtUtils-Manifest
Source0:        https://cpan.metacpan.org/authors/id/E/ET/ETHER/ExtUtils-Manifest-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  perl-generators, perl-interpreter
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(Carp), perl(Config), perl(Exporter), perl(File::Basename), perl(strict), perl(warnings)
BuildRequires:  perl(File::Copy), perl(File::Find), perl(File::Path), perl(File::Spec) >= 0.8
BuildRequires:  perl(Cwd), perl(Test::More), perl(Data::Dumper)
Requires:       perl(File::Path)

%description
Utilities to write and check a MANIFEST file.

%package        help
Summary:        Including man files for perl-ExtUtils-Manifest

%description    help
This contains man files for the using of perl-ExtUtils-Manifest.

%prep
%autosetup -n ExtUtils-Manifest-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%license LICENSE
%doc README
%{perl_vendorlib}/*

%files help
%{_mandir}/man*/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 1:1.75-2
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Jul 19 2023 leeffo <liweiganga@uniontech.com> - 1:1.75-1
- upgrade to version 1.75

* Fri Oct 28 2022 dongyuzhen <dongyuzhen@h-partners.com> - 1:1.73-2
- Rebuild for next release

* Sat Nov 13 2021 yuanxin <yuanxin24@huawei.com> - 1:1.73-1
- update version to 1.73

* Thu Jul 23 2020 xinghe <xinghe1@huawei.com> - 1:1.72-1
- update version to 1.72

* Sun Sep 15 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:1.71-4
- Package init
